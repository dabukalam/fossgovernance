# Styleguide

How to format various things in the project.

## Collection Entries

Often we need to make changes from what's captured by Zotero. That's because we use a standard format but the sites themselves may use whatever format makes the most sense for them.

* Title
    * Start with the name of the project, then a dash surrounded by spaces, then the name of the document
        * Example: `PostgreSQL - Code of Conduct`
    * Some entries aren't from specific projects but instead are general and targeted at all projects. In cases like this there's no need to add a project name to the start of the title.
    *   * Example: `Model Trademark Guidelines v1.0`
    * When captured, some titles will include extraneous information from the site page at the end of the title. Please remove this.
        * Example: `OpenMRS Logo Policy - Resources - OpenMRS Wiki` will become `OpenMRS - Logo Policy`
    * Use Title Case (or change the captured title to use it if needed)
        * Example: `LibreHealth - GOVERNANCE` will become `LibreHealth - Governance`
    * Lowercased project names should stay lowercased
        * Example: `npm` should always be lowercased
    * Spell out acronyms
        * Example: `PSF - Trademark Usage Policy` becomes `Python Software Foundation - Trademark Usage Policy`
    * Leading articles are usually optional
        * For instance, it's OK to use `Python Software Foundation` instead of `The Python Software Foundation`
        * However, sometimes the leading article is required. An example of this is `The Perl Foundation`, which is acronymed as `TPF`. In this case it wouldn't make sense to leave off the leading article.
    * Add spaces for those titles where the wiki source requires CamelCase
        * Example: `Arch Linux - TrademarkPolicy` will become `Arch Linux - Trademark Policy`
    * Text only in the title
        * Example: `🏛 Homebrew - Governance Repository` will become `Homebrew - Governance Repository`
* Snapshot
    * Each entry must include a snapshot made at the time of entry creation.
    * If Zotero doesn't automatically create a snapshop (it usually does), simply print the page to PDF and manually attach it to the entry.

