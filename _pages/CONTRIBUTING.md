---

title: Contributing to the FOSS Governance Collection
layout: single
permalink: /contribute/

---

## Code of Conduct

All activities related to the FOSS Governance project must adhere to our [Code of Conduct](/coc/). This includes issues, the IRC channel, and any other discussions or interactions.

## Licensing

All contributions must be under the [same license](https://gitlab.com/fossgovernance/fossgovernance/-/blob/master/LICENSE) as the FOSS Governance project.

## Ways to contribute

Right now there are three primary ways to contribute to the project:

1. Suggest a document to add to the collection
1. Report a problem with the collection, project, or website
1. Ask questions or provide feedback about the project

## How to contribute

Contributing is easy!

1. [Start a new issue](https://gitlab.com/fossgovernance/fossgovernance/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).
1. Choose a template for the issue. You can select from:
   * Question or other
   * Report a problem
   * Suggest a document
1. Fill in the blanks to the best of your knowledge.

🎉 Voila! You've just contributed! 🎉

## Talk to us!

Want to chat? We can do that! Drop by the `#fossgovernance` channel on freenode IRC. 

Don't have an IRC client set up? That's OK! freenode provides a handy [webchat](https://kiwiirc.com/nextclient/#irc://irc.freenode.net/#fossgovernance).

Please note that, like the rest of the project, all conversations on the IRC channel fall under our [Code of Conduct](/coc/).
